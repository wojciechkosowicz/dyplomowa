use artists;
CREATE TABLE IF NOT EXISTS artists (id INT not null auto_increment, name nvarchar(255), description varchar(100), nationality varchar(50), gender varchar(10), begin_date year, end_date year, primary key(id));
LOAD DATA LOCAL INFILE "/home/vagrant/Artists.csv"
INTO TABLE artists
COLUMNS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
ESCAPED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;
