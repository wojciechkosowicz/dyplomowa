import pymongo
from pymemcache.client import base
import time
def connectMongo():
  myclient = pymongo.MongoClient("mongodb://192.151.1.12:27017/")
  mydb = myclient["artistsdb"]
  mycol = mydb["artists"]
  return mycol

def performQuery(artistName):
  coll = connectMongo()
  result = coll.find({"DisplayName": artistName}, {"_id":0, "Nationality":1})
  for x in result:
    return x['Nationality']

def connectMemcached():
  memcachedServer = '192.151.1.11'
  memcachedPort = 11211
  client = base.Client((memcachedServer, memcachedPort))
  return client

def getCache(key):
  client = connectMemcached()
  value = client.get(key)
  return value

def setCache(key, value):
  client = connectMemcached()
  client.set(key,value)

def singleQueryTime(artist, loopTime):
  timeMongoStart = time.time()
  nationality = performQuery(artist)
  timeMongoEnd = time.time()
  artist = artist.replace(' ','')
  if loopTime == 0:
    setCache(artist, nationality)
  timeCacheStart = time.time()
  getCache(artist)
  timeCacheEnd = time.time()
  timeMongo = timeMongoEnd - timeMongoStart
  timeCache = timeCacheEnd - timeCacheStart
  returnList = [timeMongo, timeCache]
  return returnList

def profileDB():
  loopTimes = 10
  artistList = [ 'Robert Arneson', 'Laura Andreson', 'William Anthony', 'Sergei Bobrov', 'Erwan Bouroullec', 'Rob Minkoff', 'Brian Duffy', 'Richard Woods', 'Alexander Balagin', 'David Butler' ]
  for artist in artistList:
    mongoTimes = 0
    memcachedTimes = 0
    for i in range(loopTimes):
      returnTimes = singleQueryTime(artist, i)
      mongoTimes += returnTimes[0]
      memcachedTimes += returnTimes[1]
    avgMongoTime = mongoTimes/loopTimes
    avgMemcachedTime = memcachedTimes/loopTimes
    print(artist + ',' + str(avgMongoTime) + ',' + str(avgMemcachedTime))
    
profileDB()
