import time
import mysql.connector
from pymemcache.client import base

def connectMySQLServer():
  host='192.151.1.10'
  user='dyplom'
  passwd='PASSWORD'
  database='artists'
  mydb = mysql.connector.connect(host=host, user=user, passwd=passwd, database=database)
  return mydb

def performSelectQuery(artistName):
  conn = connectMySQLServer()
  cursor = conn.cursor()
  cursor.execute("Select nationality from artists where name=\"" +artistName + "\";")
  result = cursor.fetchall()
  for x in result:
    return x[0]

def connectMemcached():
  memcachedServer = '192.151.1.11'
  memcachedPort = 11211
  client = base.Client((memcachedServer, memcachedPort))
  return client

def getCache(key):
  client = connectMemcached()
  value = client.get(key)
#  print(value)
  return value

def setCache(key, value):
  client = connectMemcached()
  client.set(key,value)

def singleQueryTime(artist, loopTime):
  timeSQLStart = time.time()
  nationality = performSelectQuery(artist)
  timeSQLEnd = time.time()
  artist = artist.replace(' ','')
  if loopTime == 0:
    setCache(artist, nationality)
  timeCacheStart = time.time()
  getCache(artist)
  timeCacheEnd = time.time()
  timeSQL = timeSQLEnd - timeSQLStart
  timeCache = timeCacheEnd - timeCacheStart
  returnList = [timeSQL, timeCache]
  return returnList

def profileDB():
  loopTimes = 10
  artistList = [ 'Robert Arneson', 'Laura Andreson', 'William Anthony', 'Sergei Bobrov', 'Erwan Bouroullec', 'Rob Minkoff', 'Brian Duffy', 'Richard Woods', 'Alexander Balagin', 'David Butler' ]
  for artist in artistList:
    sqlTimes = 0
    memcachedTimes = 0
    for i in range(loopTimes):
      returnTimes = singleQueryTime(artist,i)
      sqlTimes += returnTimes[0]
      memcachedTimes += returnTimes[1]
    avgSQLTime = sqlTimes/loopTimes
    avgMemcachedTime = memcachedTimes/loopTimes
    print(artist + ',' + str(avgSQLTime) + ',' + str(avgMemcachedTime))

profileDB()  
